const { todoReducer } = require("../../components/08-useReducer/todoReducer");
const { demoTodos } = require("../fixtures/demoTodos");

describe("Pruebas en todoReducer", () => {
  test("debe de retornar el estado por defecto", () => {
    const state = todoReducer(demoTodos, {});
    expect(state).toEqual(demoTodos);
  });

  test("debe de agregar un todo", () => {
    const newTodo = {
      id: 3,
      desc: "Aprende Pepito",
      done: false,
    };

    const action = {
      type: "add",
      payload: newTodo,
    };
    const state = todoReducer(demoTodos, action);
    console.log(state);
    expect(state.length).toBe(3);
  });

  test("Debe de borrar un todo", () => {
    const newTodo = {
      id: 3,
      desc: "Aprende Pepito",
      done: false,
    };

    const action = {
      type: "delete",
      payload: newTodo.id,
    };
    const state = todoReducer(demoTodos, action);
    // console.log(state);
    expect(state.length).toBe(2);
    expect(state).toEqual(demoTodos);
  });

  test("Debe de hacer el togle del todo", () => {
    const action = {
      type: "toggle",
      payload: 2,
    };
    const state = todoReducer(demoTodos, action);
    // console.log("topgle", state);
    expect(state[1].done).toBe(true);
  });
});
