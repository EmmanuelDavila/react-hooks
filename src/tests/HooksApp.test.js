import { shallow } from "enzyme";
import "@testing-library/jest-dom";
import React from "react";
import { HooksApp } from "../HooksApp";
describe("HookApp", () => {
  test("should snapshot", () => {
    const wrapper = shallow(<HooksApp />);
    expect(wrapper).toMatchSnapshot();
  });
});
